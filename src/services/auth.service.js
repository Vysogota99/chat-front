import axios from 'axios';

const API_URL = 'http://localhost/api/v1/session/';
axios.defaults.withCredentials = true

class AuthService {
  login(user) {
    return axios
    .post(API_URL + 'login', {
      login: user.login,
      password: user.password,
    })
    .then(response => {
      const  data_to_storage = JSON.stringify(response.data)
      localStorage.setItem('user', data_to_storage);
      return response.data;
    })
  }

  logout() {
    axios
    .post(API_URL + 'logout', {})
    .then(() => {
        localStorage.removeItem('user');
      }
    )
  }

  register(user) {
    return axios
    .post(API_URL + 'sign_up', {
      name: user.name,
      lastname: user.lastname,
      datebirth: user.datebirth,
      telephone_number: user.telephone_number,
      login: user.login,
      password: user.password
    })
    .then((response) => {
      localStorage.setItem('user', JSON.stringify(response.data));
      
      return response.data;
    })
  }
}

export default new AuthService();