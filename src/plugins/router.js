import Vue from 'vue'
import VueRouter from 'vue-router'
import Blank from '../components/Blank.vue'
import NotFound from '../components/NotFound.vue'
import SignupForm from '../components/session/signup/SignupForm.vue'
import SigninForm from '../components/session/signin/SigninForm.vue'
import Chat from '../components/chats/chat/MainChat.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: '/', component: Chat,
  },
  {
    path: '/signup', component: SignupForm,
  },
  {
    path: '/login', component: SigninForm,
  },
  {
    path: '/logout', component: Blank,
  },
  {
    // 404
    path: '*', component: NotFound,
  }
]

const router = new VueRouter({
  routes,
  mode: 'history',
});

export default router;